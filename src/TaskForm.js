import React, { useState } from "react";

import { GetNow } from "./func.js";

const TaskForm = ({ addTask }) => {
  const [userInput, setUserInput] = useState("");
  const [taskDue, setTaskDue] = useState("");

  const handleTaskChange = (e) => {
    setUserInput(e.currentTarget.value);
  };
  const handleDueChange = (e) => {
    setTaskDue(e.currentTarget.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    addTask(userInput, taskDue);
    setUserInput("");
    setTaskDue("");
  };

  return (
    <form
      onSubmit={handleSubmit}
      className="flex flex-col space-y-3 sm:space-y-0 sm:flex-row sm:space-x-5"
    >
      <div className="flex flex-col justify-start">
      <label htmlFor="task" className="text-sm font-bold text-gray-400">Task name</label>
      <input
        name="task"
        type="text"
        value={userInput}
        onChange={handleTaskChange}
        placeholder="Enter task to do..."
        className="rounded-md px-6 py-3"
        required
      />
      </div>
      <div className="flex flex-col justify-start">
      <label htmlFor="enddate" className="text-sm font-bold text-gray-400">Due date</label>
      <input
        type="datetime-local"
        id="enddate"
        name="enddate"
        // value={"2014-01-02T11:42"}
        value={taskDue}
        min={GetNow()}
        onChange={handleDueChange}
        className="rounded-md px-6 py-3"
      ></input>
      </div>
      <div className="flex flex-col justify-end ">
      <label htmlFor="enddate" className="text-sm font-bold text-gray-400"></label>
      <button
      name="addtask"
        type="submit"
        className="bg-orange-400 hover:bg-orange-500 text-white font-bold px-6 py-3 rounded-md transition duration-300 hover:shadow-lg shadow-sm"
      >
        Add Task
      </button>
      </div>
    </form>
  );
};

export default TaskForm;
